package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("invalid input", Palindrome.isPalindrome("anna"));
	}
	
	@Test
	public void testIsNotPalindrome() {
		assertFalse("invalid input", Palindrome.isPalindrome("maria"));
	}
	
	@Test
	public void testBoundaryInPalindrome()
	{
		assertTrue("invalid input", Palindrome.isPalindrome("a"));
	}
	
	@Test
	public void testBoundaryOutPalindrome()
	{
		assertFalse("invalid input", Palindrome.isPalindrome("anasna"));
	}

	
	
}
